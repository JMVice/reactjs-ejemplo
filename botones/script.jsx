// Aquí se encuentra todo el código de ReactJS.


// En esta variable constante se guarda el ID en donde el contenido
// generado por ReactJS sera colocado.
const id_etiquetaDeContenidoReactJS = "root";

// Funcion que crea el botón. Su nombre a mostrar se lo pasamos por la variable props.
function Boton(props){
  return <div className="btn btn-primary">{props.nombre}</div>;
}

// Elemento ReactJS a imprimir en pantalla. Podemos llamar la función Boton y darle sus parametros
// de la forma que se muestra (<Boton nombre="Aceptar" />).
// <funcion propKey=propValue /> .
function Boton_render(){
  return (
    <div>
      <Boton nombre="Aceptar" />
      <br/>
      <Boton nombre="Actualizar" />
      <br/>
      <Boton nombre="Leer" />
      <br/>
      <Boton nombre="Eliminar" />
    </div>
  );
}

// Imprime en pantalla.
ReactDOM.render(
  <Boton_render />,
  document.getElementById(id_etiquetaDeContenidoReactJS)
);
