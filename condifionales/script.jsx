// Aquí se encuentra todo el código de ReactJS.


// En esta variable constante se guarda el ID en donde el contenido
// generado por ReactJS sera colocado.
const id_etiquetaDeContenidoReactJS = "root";

// Encansulamiento del contenido a mostrar si el checkbox esta marcado.
function CheckboxMarcado() {
  return <div className="alert alert-success">Checkbox está marcado</div>
}

// Encansulamiento del contenido a mostrar si el checkbox no esta marcado.
function CheckboxDesmarcado() {
  return <div className="alert alert-danger">Checkbox está desmarcado</div>
}

// Funcion a ejecutar en la funcion de render en ReactDOM.
function ResultadoCondicional() {
  // Captura del checkbox.
  const checkbox = document.getElementById("checkbox");

  // Condicional y retorno del contenido a mostrar.
  if (checkbox.checked) {
    return CheckboxMarcado();
  } else {
    return CheckboxDesmarcado();
  }
}

// Funcion que imprime en pantalla según la condicional y si el checkbox esta marcado
// o no.
function ImprimirEnPantalla_React() {
  ReactDOM.render(
    ResultadoCondicional(),
    document.getElementById(id_etiquetaDeContenidoReactJS)
  );
}

// Imprimimos en pantalla una vez para que no se quede la pantalla sin resultado.
// Por defecto el checkbox estara sin marcar.
ImprimirEnPantalla_React();
