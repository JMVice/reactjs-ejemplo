// Aquí se encuentra todo el código de ReactJS.


// En esta variable constante se guarda el ID en donde el contenido
// generado por ReactJS sera colocado.
const id_etiquetaDeContenidoReactJS = "root";

// Funcion que crea un nuevo elemento. En este caso pinta en el HTML
// la hora actual.
function reloj() {
  ReactDOM.render(
    new Date().toLocaleTimeString(),
    document.getElementById(id_etiquetaDeContenidoReactJS)
  );
}

// Intervalo de 1 segundo para que se actualize la hora mostrada en pantalla.
setInterval(() => {
  reloj();
}, 1000);