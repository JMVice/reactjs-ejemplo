// Aquí se encuentra todo el código de ReactJS.


// En esta variable constante se guarda el ID en donde el contenido
// generado por ReactJS sera colocado.
const id_etiquetaDeContenidoReactJS = "root";

const contenidoReactJS = <div></div>;

function Enlace(){
  function click(e){
    // Evita que la página se recargue.
    e.preventDefault();
    // Alerta del explorador.
    alert("Has pulsado el link.");
  }

  // Elemento a mostrar en pantalla.
  return (
    <a href="#" onClick={click}>Desplegar alerta.</a>
  )
}

// Imprimimos el contenido en el HTML.
ReactDOM.render(
  <Enlace />, // Pasamos la funcion Enlace().
  document.getElementById(id_etiquetaDeContenidoReactJS)
);
