// Aquí se encuentra el código de ReactJS.

// En esta variable constante se guarda el ID en donde el contenido
// generado por ReactJS sera colocado.
const id_etiquetaDeContenidoReactJS = "root";

// Contenido HTML a pintar con el uso de ReactJs.
const contenidoReactJS = (
  <ul>
    <li>
      <a href="./reloj">Reloj - Actualización de elemento</a>
    </li>
    <li>
      <a href="./botones">Botones - Componentes y propiedades</a>
    </li>
    <li>
      <a href="./eventos">Eventos</a>
    </li>
    <li>
      <a href="./condifionales">Condicionales</a>
    </li>
  </ul>
);

// Pinta en pantalla el contenido de la variable constante contenidoReactJS .
ReactDOM.render(
  contenidoReactJS,
  document.getElementById(id_etiquetaDeContenidoReactJS)
);
