// Aquí se encuentra todo el código de ReactJS.


// En esta variable constante se guarda el ID en donde el contenido
// generado por ReactJS sera colocado.
const id_etiquetaDeContenidoReactJS = "root";

const contenidoReactJS = <div></div>;

ReactDOM.render(
  contenidoReactJS,
  document.getElementById(id_etiquetaDeContenidoReactJS)
);
